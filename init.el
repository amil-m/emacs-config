;;;; Initialisation --------------------------------------------------
;; Increase garbage collection threshold
;; https://github.com/jwiegley/dot-emacs/blob/814345f/init.el
(defvar file-name-handler-alist-old file-name-handler-alist)
(setq file-name-handler-alist   nil
      message-log-max           16384
      gc-cons-threshold         402653184
      gc-cons-percentage        0.6)
(add-hook 'after-init-hook
          (lambda ()
            (setq file-name-handler-alist file-name-handler-alist-old
                  gc-cons-threshold 16777216
                  gc-cons-percentage 0.1)
            (garbage-collect))
          t)

;; Increase the amount of data which Emacs reads from the process
(setq read-process-output-max (* 1024 1024))

;; Do not resize frame to number of columns
(setq frame-inhibit-implied-resize t)

;; Initialise melpa and use-package for package management
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


;;;; Visual Tweaks ---------------------------------------------------
;; Do not show the startup screen.
(setq inhibit-startup-message t)

;; Empty scratch message
(setq initial-scratch-message nil)

;; Mute the bell
(setq ring-bell-function 'ignore)

;; Hide the toolbar
(tool-bar-mode -1)

;; Hide the menu bar if not on a mac
(when (not (eq system-type 'darwin))
  (menu-bar-mode -1))

;; Hide the scrollbar
(scroll-bar-mode -1)

;; Modeline Colours
;; (set-face-attribute 'mode-line nil :background "#4e6a8c" :foreground "#FDF1CB")
;; (set-face-attribute 'mode-line-inactive nil :background "#5a6869" :foreground "a3b4cc")

;; Terminal colours for old theme from ansi-color-names-vector
(with-eval-after-load 'term
  (setq eterm-256color-disable-bold nil)
  (set-face-attribute 'term-color-black nil
                      :foreground (face-foreground 'default)
                      :background (face-foreground 'default))
  (set-face-attribute 'term-color-white nil
                      :foreground "white"
                      :background "white")
  (set-face-attribute 'term-color-blue nil
                      :foreground (elt ansi-color-names-vector 4)
                      :background "#BBDEFB")
  (set-face-attribute 'term-color-cyan nil
                      :foreground (elt ansi-color-names-vector 6)
                      :background "#B2EBF2")
  (set-face-attribute 'term-color-green nil
                      :foreground (elt ansi-color-names-vector 2)
                      :background "#C8E6C9")
  (set-face-attribute 'term-color-magenta nil
                      :foreground (elt ansi-color-names-vector 5)
                      :background "#E1BEE7")
  (set-face-attribute 'term-color-red nil
                      :foreground (elt ansi-color-names-vector 1)
                      :background "#FFCDD2")
  (set-face-attribute 'term-color-yellow nil
                      :foreground (elt ansi-color-names-vector 3)
                      :background "#FFF9C4"))

;; Mac transparent toolbar
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . light))

;; Hide the fringe where the arrows are shown for truncated lines
(fringe-mode '(0 . 0))

;; Set window margins to compensate for no fringe
(defun my/set-margins ()
(setq-default left-margin-width 2 right-margin-width 2)
(set-window-buffer nil (current-buffer)))
(my/set-margins)

;; Fonts
;; Set the line spacing
(setq-default line-spacing 0.1)

;; Set faces
(set-face-attribute 'default nil :font "JetBrains Mono" :weight 'light :height 140)
(set-face-attribute 'fixed-pitch nil :font "JetBrains Mono" :weight 'light :height 140)
(set-face-attribute 'variable-pitch nil :font "Iosevka Aile" :weight 'light :height 1.3)
(set-face-attribute 'italic nil :font "JetBrains Mono" :slant 'italic)

;; Emoji Fonts
(if (eq system-type 'darwin)
    (set-fontset-font t 'symbol "Apple Color Emoji")
  (set-fontset-font t 'symbol "Noto Color Emoji" nil 'append))

;; Use spaces, not tabs, for indentation
(setq-default indent-tabs-mode nil)

;; Display the distance between two tab stops as 4 characters wide
(setq-default tab-width 4)

;; # Key on macOS
(when (eq system-type 'darwin)
 (global-set-key (kbd "M-3") '(lambda () (interactive) (insert "#"))))


;;;; System Settings -------------------------------------------------
;; Personal info
(setq user-full-name "Amil Mohanan"
      user-mail-address "amil@mohanan.net")

;; Write backup files to their own directory
(make-directory "~/.emacs.d/backup/" t)
(setq backup-directory-alist '((".*" . "~/.emacs.d/backup"))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; how many of the newest versions to keep
      kept-old-versions 5    ; and how many of the old
      )

;; Save autosave files in the .emacs.d folder
(make-directory "~/.emacs.d/auto-save/" t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save/" t)))

;; Remember my position in a file
(save-place-mode t)
(setq save-place-file "~/.emacs.d/saveplace")

;; Do not move the current file while creating backup.
(setq backup-by-copying t)

;; Cleanup whitespaces before each save
(add-hook 'before-save-hook 'whitespace-cleanup)

;; Enable better scrolling
(pixel-scroll-precision-mode)

;; Don't write lock-files because its single user
(setq create-lockfiles nil)

;; Automatically update buffer if file changes
(global-auto-revert-mode 1)

;; Auto refresh dired when file changes
(add-hook 'dired-mode-hook 'auto-revert-mode)

;; Always refresh the file when it changes
(global-auto-revert-mode t)

;; Write customizations to a separate file instead of this file
(setq custom-file (concat user-emacs-directory "custom.el"))

;; Set the default find file location to the home directory
(setq default-directory "~/")

;; Less information in dired listings
(setq dired-listing-switches "-agho --group-directories-first")

;; Delete files by moving them to trash
(setq delete-by-moving-to-trash t)

;; Use command key as control on MacOS
(when (eq system-type 'darwin) (setq mac-command-modifier 'control))

;; Fix for dired to work on MacOS
(when (eq system-type 'darwin)
  (require 'ls-lisp)
  (setq ls-lisp-use-insert-directory-program nil))

;; Ensures environment variables inside Emacs look the same as in
;; the user's shell.
(use-package exec-path-from-shell
  :ensure t
  :if (memq window-system '(mac ns x))
  :config
  (setq exec-path-from-shell-variables '("PATH" "GOPATH"))
  (exec-path-from-shell-initialize))

;; Use eshell with fish
(when (file-name-nondirectory (getenv "SHELL")) "fish"
      (setenv "ESHELL" "fish"))

;; Use ZSH in macOS
(when (eq system-type 'darwin)
  (setq shell-file-name "zsh")
  (setq explicit-shell-file-name "/bin/zsh")
  (setq explicit-zsh-args '("--interactive" "--login")))

;; Disble company mode in eshell
(add-hook 'eshell-mode-hook
          (lambda () (company-mode -1)))


;;;; Workspace Settings ----------------------------------------------
;; Wrap lines at 80 characters wide, not 70
(setq fill-column 80)

;; When on a tab, make the cursor the tab length
(setq-default x-stretch-cursor t)

;; Delete white space at end of files
(add-hook 'before-save-hook 'whitespace-cleanup)

;; Disable arrow keys
(global-unset-key (kbd "<left>"))
(global-unset-key (kbd "<right>"))
(global-unset-key (kbd "<up>"))
(global-unset-key (kbd "<down>"))

;; Teach me shortcuts
(setq suggest-key-bindings t)

;; Preserve the position of the cursor when scrolling
(setq scroll-preserve-screen-position 'always)

;; Turn on highlight matching brackets when cursor is on one
(setq show-paren-delay 0)
(show-paren-mode 1)

;; Auto-close brackets and double quotes
(electric-pair-mode 1)

;; Add system clipboard to emacs kill ring
(setq save-interprogram-paste-before-kill t)

;; Pulse current line on window switch
;; https://karthinks.com/software/batteries-included-with-emacs/#fn:1
(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(scroll-up-command
                   scroll-down-command
                   recenter-top-bottom
                   other-window))
  (advice-add command :after #'pulse-line))

;; Ask y/n instead of yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;; Always refresh the file when it changes
(global-auto-revert-mode t)

;; Have window buffer menu automatically have focus when C-x C-b
(define-key global-map [remap list-buffers] 'buffer-menu-other-window)

;; Switch to minibuffer function
(defun switch-to-minibuffer-window ()
  "switch to minibuffer window (if active)"
  (interactive)
  (when (active-minibuffer-window)
    (select-window (active-minibuffer-window))))

;; Assign switch-to-minibuffer to C-§ (Mac) or C-` (Linux)
(if (eq system-type 'darwin)
    (global-set-key (kbd "C-§") 'switch-to-minibuffer-window)
  (global-set-key (kbd "C-`") 'switch-to-minibuffer-window))

;; Bind recent documents to C-x C-r
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key (kbd "C-x C-r") 'recentf-open-files)

;; Change default open read only to C-x r (default is C-x C-r)
(global-set-key (kbd "C-x r <return>") 'find-file-read-only)

;; Enable highlighted line in recentf
(add-hook 'recentf-dialog-mode-hook 'hl-line-mode)

;; CUA Selection Mode
;; Use C-Enter for special multiline selection and movement
(cua-selection-mode 1)

;; Switch to last buffer with M-o
(global-set-key (kbd "M-o")  'mode-line-other-buffer)

;; When splitting windows, move to the new window automatically
(defun king/split-window-below-and-switch ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))

(defun king/split-window-right-and-switch ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))

(global-set-key (kbd "C-x 2") 'king/split-window-below-and-switch)
(global-set-key (kbd "C-x 3") 'king/split-window-right-and-switch)

;; Hide ^M on files from Windows
(defun remove-dos-eol ()
  "Do not show ^M in files containing mixed UNIX and DOS line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))
;; Apply to text modes and gnus (used to show mail)
(add-hook 'text-mode-hook 'remove-dos-eol)
(add-hook 'gnus-article-mode-hook 'remove-dos-eol)

;; Simple Modeline
(use-package simple-modeline
  :ensure t
  :hook (after-init . simple-modeline-mode))

;; Neotree [F8]
;; Tree interface for browsing directories
(use-package neotree
  :ensure t
  :init
  (global-set-key [f8] 'neotree-toggle))

;; Vertico - Vertical Interactive Completion
;; https://github.com/minad/vertico
(use-package vertico
  :ensure t
  :defer t
  :init
  (vertico-mode)
  ;; Different scroll margin
  (setq vertico-scroll-margin 0)
  ;; Show more candidates
  (setq vertico-count 10)
  ;; Grow and shrink the Vertico minibuffer
  (setq vertico-resize t)
  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :ensure t
  :init
  (savehist-mode))

;; Orderless - completion style that matches candidates that match components in any order
;; https://github.com/oantolin/orderless
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

;; Enable fuzzy minibuffer matching to go with selectrum
;; https://karthinks.com/software/more-batteries-included-with-emacs/
(setq completion-styles '(initials partial-completion flex))
(setq completion-cycle-threshold 10)

;; Enable richer annotations using the Marginalia package
;; https://github.com/minad/marginalia
(use-package marginalia
  :ensure t
  :config
  (marginalia-mode))

;; Adaptive Wrap
;; Install adaptive-wrap to take care of wrapping long lines
(use-package adaptive-wrap
  :ensure t
  :init (setq-default adaptive-wrap-extra-indent 2)
  :hook ((web-mode . adaptive-wrap-prefix-mode)
         (visual-line-mode-hook . adaptive-wrap-prefix-mode)))

;; Enable visual-line-mode for wrapping
(global-visual-line-mode +1)

;; Buffer Move
;; https://github.com/lukhas/buffer-move/
(use-package buffer-move
  :ensure t
  :init
  (global-set-key (kbd "<C-s-up>")     'buf-move-up)
  (global-set-key (kbd "<C-s-down>")   'buf-move-down)
  (global-set-key (kbd "<C-s-left>")   'buf-move-left)
  (global-set-key (kbd "<C-s-right>")  'buf-move-right))

;; Undo Tree
;; C-x u    undo-tree-visualize
;; C-x r U  undo-tree-restore-state-from-register
;; (use-package undo-tree
;;   :ensure t
;;   :init
;;   (global-undo-tree-mode))


;;;; Spell Checking --------------------------------------------------
;; Install aspell first (brew install aspell)
(setq ispell-program-name "aspell")
(setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_GB"))

;; Set dictionary
;; (setq ispell-dictionary "british")

;; Order corrections by likeliness
(setq flyspell-sort-corrections nil)

;; Dont print messages for every wrong word
(setq flyspell-issue-message-flag nil)

;; Dont ask to save when adding new words to dictionary using M-$ i
(setq ispell-silently-savep t)

;; Switch on spell check for text modes
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook 'flyspell-mode)

;; Skip spell check in source code in org mode
(add-to-list 'ispell-skip-region-alist '("^#+BEGIN_SRC" . "^#+END_SRC"))

;; Flyspell-popup https://github.com/xuchunyang/flyspell-popup
(use-package flyspell-popup
  :ensure t
  :hook ('flyspell-mode-hook #'flyspell-popup-auto-correct-mode))

;; Call popup when correcting with flyspell
(advice-add 'flyspell-auto-correct-word
            :before #'flyspell-popup-correct)

;; Add advice to flyspell-goto-next-error to make it flash errors
;; based on pulse-momentary-highlight-line from pulse.el
(defun pulse-momentary-highlight-word (point &optional face)
  "Highlight the word at start of point.
Optional argument FACE specifies the face to do the highlighting."
  (save-excursion
    ;; flyspell-goto-next-error always takes you to start of word
    (let ((start (goto-char point))
          (end (save-excursion
                 (forward-word)
                 (point))))
      (pulse-momentary-highlight-region start end face))))

(defun pulse-word (&rest _)
  "Pulse the current word."
  (pulse-momentary-highlight-word (point)))

(advice-add 'flyspell-goto-next-error
            :after
            'pulse-word)

(bind-key "C-," 'flyspell-goto-next-error)
(bind-key "C-." 'flyspell-correct-word)

;; writegood-mode (https://github.com/bnbeckwith/writegood-mode)
;; Minor mode for Emacs to improve English writing
;; To get reading score: M-x writegood-reading-ease
(use-package writegood-mode
  :ensure t
  ;;:config (add-to-list 'writegood-weasel-words "actionable")
  )

;; Switch on writegood for text modes
(add-hook 'text-mode-hook 'writegood-mode)
(add-hook 'org-mode-hook 'writegood-mode)

;; Hungry Delete - Deletes whitespace to next character
;; https://github.com/nflath/hungry-delete
(use-package hungry-delete
  :ensure t
  :config
  (global-hungry-delete-mode)
  (set-variable 'hungry-delete-join-reluctantly t))


;;;; Programming -----------------------------------------------------
;; Indentation settings
(setq c-basic-offset 4)
(setq js-indent-level 2)
(setq css-indent-offset 2)

;; Magit is a complete text-based user interface to Git.
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

;; Multiple Cursors
;; https://github.com/magnars/multiple-cursors.el
;; To get out of multiple-cursors-mode, press <return> or C-g.
;; The latter will first disable multiple regions before disabling
;; multiple cursors. If you want to insert a newline in
;; multiple-cursors-mode, use C-j, but I have set to return.
(use-package multiple-cursors
  :ensure t
  :init
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
  ;; Add cursor on click to M-Right Click
  (global-unset-key (kbd "M-<down-mouse-1>"))
  (global-set-key (kbd "M-<mouse-1>") 'mc/add-cursor-on-click))

;; Make return new line instead of C-j in multiple-cursors
;; This set inside multiple-cursors-core when using use-package
(use-package multiple-cursors-core
  :bind
  (:map mc/keymap
        ("<return>" . nil)))

;; Toggle line numbers using F7
;; https://protesilaos.com/codelog/2020-07-17-emacs-mixed-fonts-org/
(use-package display-line-numbers
  :defer
  :config
  ;; Set absolute line numbers.  A value of "relative" is also useful.
  (setq display-line-numbers-type t)

  (define-minor-mode prot/display-line-numbers-mode
    "Toggle `display-line-numbers-mode' and `hl-line-mode'."
    :init-value nil
    :global nil
    (if prot/display-line-numbers-mode
        (progn
          (display-line-numbers-mode 1)
          (hl-line-mode 1))
      (display-line-numbers-mode -1)
      (hl-line-mode -1)))
  :bind ("<f7>" . prot/display-line-numbers-mode))

;; Expand Region - increase selected region by semantic units.
;; https://github.com/magnars/expand-region.el
(use-package expand-region
  :ensure t
  :init
  (global-set-key (kbd "C-=") 'er/expand-region))

;;; HTML
;; Web-mode (https://web-mode.org/)
;; Major mode for editing web templates.
(use-package web-mode
  :ensure t
  :mode ("\\.html?\\'"
         "\\.svelte?\\'"
         "\\.[jt]sx?\\'")
  :custom
  (web-mode-attr-indent-offset 2)
  (web-mode-enable-css-colorization t)
  (web-mode-enable-auto-closing t)
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-enable-current-element-highlight t)
  ;; svelte support
  (web-mode-engines-alist
   '(("svelte" . "\\.svelte\\'"))))

;; Auto-Rename-Tag - rename matching HTML tags automatically
(use-package auto-rename-tag
  :ensure t
  :hook (web-mode . auto-rename-tag-mode))

;; Rainbow Mode - show HTML colours in colour
(use-package rainbow-mode
  :ensure t
  :hook ((web-mode . rainbow-mode)
         (css-mode . rainbow-mode)))

;; Apheleia - Auto code formatting
;; https://github.com/radian-software/apheleia
(use-package apheleia
  :ensure t
  :config
  (apheleia-global-mode +1))

;; Emmet Mode
(use-package emmet-mode
  :ensure t
  :after (web-mode css-mode scss-mode)
  :config
  (setq emmet-move-cursor-between-quotes t)
  (add-hook 'emmet-mode-hook (lambda () (setq emmet-indent-after-insert nil)))
  (add-hook 'sgml-mode-hook 'emmet-mode)
  (add-hook 'css-mode-hook  'emmet-mode)
  (unbind-key "C-M-<left>" emmet-mode-keymap)
  (unbind-key "C-M-<right>" emmet-mode-keymap)
  :bind ("C-j" . emmet-expand-line))

;; Company mode
(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0
        company-minimum-prefix-length 3
        company-minimum-prefix-length 1)
  (global-company-mode t))

;; LSP - Elgot
;; https://github.com/joaotavora/eglot
(use-package eglot
  :ensure t)

;; Tree Sitter - Code parsing system
;; https://emacs-tree-sitter.github.io/
(use-package tree-sitter
  :ensure t
  :config
  ;; activate tree-sitter on any buffer containing code for which it has a parser available
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(use-package tree-sitter-langs
  :ensure t
  :after tree-sitter)

;; Conda
(use-package conda
  :ensure t
  :init
  (setq conda-anaconda-home (expand-file-name "~/miniconda3"))
  (setq conda-env-home-directory (expand-file-name "~/miniconda3"))
  :config
  (conda-env-initialize-interactive-shells)
  (conda-env-initialize-eshell))

;; Babel ORG
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)))

;; Do not ask for confirmation when evaluating babel
(setq org-confirm-babel-evaluate nil)

;;;; Other Packages --------------------------------------------------
;; Elfeed RSS
(use-package elfeed
  :ensure t)

;; use an org file to organise feeds
(use-package elfeed-org
  :ensure t
  :config
  (elfeed-org)
  (setq rmh-elfeed-org-files (list "~/Sync/elfeed.org")))

;; Markdown Mode
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))


;;;; ORG mode --------------------------------------------------------
;; Fix the indentation
(add-hook 'org-mode-hook 'org-indent-mode)

;; Change ... to downward arrow for Headers with collapsed content
(setq org-ellipsis  " ▾")

;; Hide the syntax for emphasis (/_* etc)
(setq org-hide-emphasis-markers t)

;; Fix the spacing around closed org-mode headers
(customize-set-variable 'org-blank-before-new-entry
                        '((heading . nil)
                          (plain-list-item . nil)))
(setq org-cycle-separator-lines 1)

;; Turn off truncate-lines so that lines don't get cut when using org
(setq org-startup-truncated nil)

;; Indent source code inside org
(setq org-src-tab-acts-natively t)

;; Syntax highlighting inside org code blocks when exporting HTML
(setq org-src-fontify-natively t)

;; Show images in org-mode
(setq org-display-inline-images t)

;; Show images at 550px wide
(setq org-image-actual-width (list 550))

;; Load images by default
(setq org-display-inline-images t)
(setq org-redisplay-inline-images t)
(setq org-startup-with-inline-images t)

;; Sentences end with a single space
(setq sentence-end-double-space nil)

;; Fontify quotes
(setq org-fontify-quote-and-verse-blocks t)

;; Ensure indenting of headings
(require 'org-indent)

;; Set org headings
(require 'org-faces)

;; Increase the size and set font of org headings
(set-face-attribute 'org-document-title nil :font "Iosevka Aile" :weight 'bold :height 1.3)
(dolist (face '((org-level-1 . 1.2)
                (org-level-2 . 1.1)
                (org-level-3 . 1.05)
                (org-level-4 . 1.0)
                (org-level-5 . 1.1)
                (org-level-6 . 1.1)
                (org-level-7 . 1.1)
                (org-level-8 . 1.1)))
  (set-face-attribute (car face) nil :font "Iosevka Aile" :weight 'medium :height (cdr face)))

;; Set fixed-pitch fonts in org-mode
(set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-table nil  :inherit 'fixed-pitch)
(set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

;; Get rid of the background on column views
(set-face-attribute 'org-column nil :background nil)
(set-face-attribute 'org-column-title nil :background nil)

;; org-bullets for fancy heading bullets
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
  :custom
  (org-bullets-bullet-list '( "◉" "○" "●" "◎" "◌")))

;; org-re-reveal - export reveal.js files from org
;; https://gitlab.com/oer/org-re-reveal
(use-package org-re-reveal
  :ensure t
  :config
  (setq org-re-reveal-revealjs-version "4")
  (setq org-re-reveal-transition "slide"))

;; org-present: minimalist presentation tool for org-mode
;; https://github.com/rlister/org-present
(use-package org-present
  :ensure t)

;; Hook functions to start visual-fill-mode when presentation
;; https://systemcrafters.net/emacs-tips/presentations-with-org-present/
(defun my/org-present-start ()
  ;; Center the presentation and wrap lines
  (setq visual-fill-column-width 110)
  (visual-fill-column-mode 1)
  ;; Hide cursor
  (org-present-hide-cursor)
  ;; Hide modeline
  (setq mode-line-format nil)
  ;; Set a blank header line string to create blank space at the top
  (setq header-line-format " "))

(defun my/org-present-end ()
  ;; Stop centering the document and reset width
  (setq visual-fill-column-width 80)
  (visual-fill-column-mode 0)
  ;; Show cursor
  (org-present-show-cursor)
  ;; Clear the header line format by setting to `nil'
  (setq header-line-format nil)
  ;; Reset window fringe
  (my/set-margins)
  ;; Set the mode-line back to original settings from simple-modeline
  (setq mode-line-format simple-modeline--mode-line))

;; Register hooks with org-present
(add-hook 'org-present-mode-hook 'my/org-present-start)
(add-hook 'org-present-mode-quit-hook 'my/org-present-end)

;; Visual-fill-mode centers text
;; Includes settings for focus-mode minor mode
;; https://github.com/joostkremers/visual-fill-column
(use-package visual-fill-column
  :ensure t
  :config
  (setq visual-fill-column-width 80
        visual-fill-column-center-text t)
  ;; Minor mode to toggle visual-fill-column and hide modeline
  ;; https://protesilaos.com/codelog/2020-07-16-emacs-focused-editing/
  (define-minor-mode focus-mode
    "Toggle visual-fill-mode and hide the modeline in non programming modes"
    :init-value nil
    :global nil
    (if focus-mode
        (progn
          ;; Turn on focus-mode
          (visual-fill-column-mode 1)
          ;; Unless its in a programming-mode hide the mode-line
          (unless (derived-mode-p 'prog-mode)
            ;; Turn on writegood-mode
            (writegood-mode 1)
            ;; Hide modeline
            (setq mode-line-format nil)))

      ;; Disable focus-mode
      (visual-fill-column-mode 0)
      ;; Reset window fringe
      (my/set-margins)
      (unless (derived-mode-p 'prog-mode)
        ;; Disable writegood-mode
        (writegood-mode -1)
        ;; Set the mode-line back to original settings from simple-modeline
        (setq mode-line-format simple-modeline--mode-line))))
  :bind ("C-c f" . focus-mode))


;;;; Custom Functions ------------------------------------------------
;; Insert links autmatically if text is selcted, grab urls from
;; clipboard and titles from webpage urls
;; https://xenodium.com/emacs-dwim-do-what-i-mean/
(defun ar/org-insert-link-dwim ()
  "Like `org-insert-link' but with personal dwim preferences."
  (interactive)
  (let* ((point-in-link (org-in-regexp org-link-any-re 1))
         (clipboard-url (when (string-match-p "^http" (current-kill 0))
                          (current-kill 0)))
         (region-content (when (region-active-p)
                           (buffer-substring-no-properties
                            (region-beginning)
                            (region-end)))))
    (cond ((and region-content clipboard-url (not point-in-link))
           (delete-region (region-beginning) (region-end))
           (insert (org-make-link-string clipboard-url region-content)))
          ((and clipboard-url (not point-in-link))
           (insert
            (org-make-link-string
             clipboard-url
             (read-string "title: "
                          (with-current-buffer
                              (url-retrieve-synchronously clipboard-url)
                            (dom-text (car (dom-by-tag
                                            (libxml-parse-html-region
                                             (point-min)
                                             (point-max))
                                            'title))))))))
          (t (call-interactively 'org-insert-link)))))

(define-key org-mode-map (kbd "C-c C-l") nil) ; remove org-insert-link binding
(define-key org-mode-map (kbd "C-c C-l") 'ar/org-insert-link-dwim) ; set key


;; Title capitalisation - Capitalises words automatically
;; http://www.Karl-Voit.at/2015/05/25/elisp-title-capitalization/
(defun title-capitalization (beg end)
  "Proper English title capitalization of a marked region"
  ;; - before: the presentation of this heading of my own from my keyboard and yet
  ;; - after:  The Presentation of This Heading of My Own from My Keyboard and Yet
  ;; - before: a a a a a a a a
  ;; - after:  A a a a a a a A
  (interactive "r")
  (save-excursion
    (let* (
           ;; basic list of words which don't get capitalized according to simplified rules:
           ;; http://karl-voit.at/2015/05/25/elisp-title-capitalization/
           (do-not-capitalize-basic-words '("a" "ago" "an" "and" "as" "at" "but" "by" "for"
                                            "from" "in" "into" "it" "next" "nor" "of" "off"
                                            "on" "onto" "or" "over" "past" "so" "the" "till"
                                            "to" "up" "yet"
                                            "n" "t" "es" "s"))
           ;; if user has defined 'my-do-not-capitalize-words, append to basic list:
           (do-not-capitalize-words (if (boundp 'my-do-not-capitalize-words)
                                        (append do-not-capitalize-basic-words my-do-not-capitalize-words )
                                      do-not-capitalize-basic-words
                                      )
                                    )
           )
      ;; go to begin of first word:
      (goto-char beg)
      (capitalize-word 1)
      ;; go through the region, word by word:
      (while (< (point) end)
        (skip-syntax-forward "^w" end)
        (let ((word (thing-at-point 'word)))
          ;;        (let ((word (thing-at-point 'word t)))
          (if (stringp word)
              ;; capitalize current word except it is list member:
              (if (member (downcase word) do-not-capitalize-words)
                  (downcase-word 1)
                (capitalize-word 1)))))
      ;; capitalize last word in any case:
      (backward-word 1)
      (if (and (>= (point) beg)
               (not (member (or (thing-at-point 'word t) "s")
                            '("n" "t" "es" "s"))))
          (capitalize-word 1)))))


;; Fill to end of line with dashes
(defun fill-dashes-to-end ()
  "Fill dashes to the end of the 'fill-column'"
  (interactive)
  (save-excursion
    (end-of-line)
    (while (< (current-column) fill-column)
      (insert-char ?-))))


;; Use a pop-up window for a bottom eshell
;; https://github.com/stanhe/pop-eshell
(defconst my-eshell " *Popup-Shell*" "my shell name, use eshell.")
(defvar pre-path nil "previous directory.")

(defun get-current-directory (&optional buffer)
  "get current directory."
  (if buffer
      (with-current-buffer buffer
        (file-name-directory (or (buffer-file-name) default-directory)))
    (file-name-directory (or (buffer-file-name) default-directory))))

(defun eshell-pop-bottom()
  "pop eshell at bottom"
  (let ((pos-buffer (current-buffer))
        (tmp-eshell (get-buffer my-eshell))
        (dir (get-current-directory)))
    ;;check if my-eshell exists, if not create one.
    (unless tmp-eshell
      (setq tmp-eshell (eshell 100))
      (with-current-buffer tmp-eshell
        (eshell/clear-scrollback)
        (rename-buffer my-eshell)
        (switch-to-buffer pos-buffer)))
    (setq window
          (select-window
           (display-buffer-in-side-window tmp-eshell '((side . bottom))) t))
    (set-window-dedicated-p window t)
    (when (not (equal pre-path dir))
      (eshell/cd dir)
      (eshell-send-input)
      (setq pre-path dir))))

(defun eshell-pop-toggle ()
  "pop or hide bottom eshell side window."
  (interactive)
  (if (get-buffer-window my-eshell)
      (delete-windows-on my-eshell)
    (eshell-pop-bottom)))

(global-set-key (kbd "C-c t") 'eshell-pop-toggle)


;; Narrow or widen buffer with C-x n
;; http://endlessparentheses.com/emacs-narrow-or-widen-dwim.html
(defun narrow-or-widen-dwim (p)
  "Widen if buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree, or
defun, whichever applies first. Narrowing to
org-src-block actually calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer
is already narrowed."
  (interactive "P")
  (declare (interactive-only))
  (cond ((and (buffer-narrowed-p) (not p)) (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning)
                           (region-end)))
        ((derived-mode-p 'org-mode)
         ;; `org-edit-src-code' is not a real narrowing
         ;; command. Remove this first conditional if
         ;; you don't want it.
         (cond ((ignore-errors (org-edit-src-code) t)
                (delete-other-windows))
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        ((derived-mode-p 'latex-mode)
         (LaTeX-narrow-to-environment))
        (t (narrow-to-defun))))

(define-key ctl-x-map "n" #'narrow-or-widen-dwim)
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (define-key LaTeX-mode-map "\C-xn"
                        nil)))
