### My settings for Emacs

My dot files for emacs. Custom shortcuts are listed below.

Custom shortcuts              | Action
------------------------------|-----------------------
`M-o`                         | Switch to last buffer
`F8`                          | Neotree
`F7`                          | Toggle Line Numbers
`C-§`                         | Switch to minibuffer
`C-x C-r`                     | Recent documents list
`C-x r [return]`              | Read only open
`C-c f`                       | Focus Mode
`C-c C-e C-e`                 | Popup eshell

Plugins                | Shortcut
-----------------------|---------------------------------
`C-x C-m`              | Mu4E
`C-Super-[arrow keys]` | Move window buffer (Buffer move)

Multiple Cursors      | Shortcut
----------------------|-------------------------------
Select text and `C->` | Add cursor to next occurrence
Select text and `C-<` | Add cursor to prev occurrence
`C-c C-<`             | Mark all occurrences
`M-[mouse-1]`         | Add a cursor to click
`C-g C-g`             | Get out of multiple cursors

Undo Tree   | Shortcut
------------|---------------------
`C-x u`     | Undo tree visualise
`C-x r u`   | Restore state

Expand Region | Shortcut
--------------|------------------
`C-=`         | Expand region
`C--`         | Contract region

Flyspell | Shortcut
---------|-----------------------
`C-,`    | Next spelling error
`C-.`    | Correct spelling error

Magit   | Shortcut
--------|-------------
`C-x g` | Magit status
