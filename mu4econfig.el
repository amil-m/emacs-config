;; Add to init.el:

;;;; MU4E ----------------------------------------------------------------------
  ;; (defun load-if-exists (f)
  ;;   "load the elisp file only if it exists and is readable"
  ;;   (if (file-readable-p f)
  ;;   (load-file f)))

  ;; (load-if-exists "~/.emacs.d/mu4econfig.el")


;; http://www.macs.hw.ac.uk/~rs46/posts/2014-01-13-mu4e-email-client.html

(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu/mu4e/")
(require 'mu4e)

;; Set C-x C-m to open mu4e
(global-set-key "\C-x\ \C-m" 'mu4e)

(setq mu4e-maildir (expand-file-name "~/.Maildir"))

(setq mu4e-drafts-folder "/Drafts")
(setq mu4e-sent-folder   "/Sent Items")
(setq mu4e-trash-folder  "/Trash")
(setq mu4e-refile-folder "/Archive")

;; This fixes issue where mail was not being archived
(setq mu4e-change-filenames-when-moving t)

; get mail
(setq mu4e-get-mail-command "mbsync -c ~/.emacs.d/.mbsyncrc fastmail"
      mu4e-update-interval 300
      mu4e-headers-auto-update t
      mu4e-index-update-in-background t)

;; Set shortcuts for the jump menu
(setq mu4e-maildir-shortcuts
      '( ("/INBOX"        . ?i)
     ("/Archive"      . ?a)
     ("/Sent Items"   . ?s)
     ("/Trash"        . ?t)
     ("/Drafts"       . ?d)))


;; SMTP Settings -----
;; Authentication is saved in ~/.authinfo;
(require 'smtpmail)
(setq
   message-send-mail-function   'smtpmail-send-it
   smtpmail-default-smtp-server "smtp.fastmail.com"
   smtpmail-smtp-server         "smtp.fastmail.com"
   smtpmail-smtp-service        465
   smtpmail-stream-type         'ssl)

;; report problems with the smtp server
(setq
 smtpmail-debug-info t
 smtpmail-debug-verb t)

;; Set up queue for offline email (use mu, mkdir  ~/Maildir/queue to set up first)
(setq smtpmail-queue-mail nil  ;; start in normal mode
      smtpmail-queue-dir   "~/.Mailqueue/cur")

;; Fix GPG Issue of not decrypting SMTP password
(setf epa-pinentry-mode 'loopback)

;; It maybe possible to use Keychain for SMTP password as well
;; https://lists.gnu.org/archive/html/help-gnu-emacs/2014-11/msg00480.html




;; Other Settings -----
;; Set mu4e as the default mail program in emacs
(setq mail-user-agent 'mu4e-user-agent)

;; Sync changes on exiting Mu4E and do it in background
(advice-add 'mu4e-quit
        :before
        (lambda () (mu4e-update-mail-and-index
            mu4e-index-update-in-background)))

;; Sync changes after marks are applied
(advice-add 'mu4e-mark-execute-all
        :after
        'mu4e-update-mail-and-index)

;; Automatically apply marks on messages
(setf mu4e-headers-leave-behavior 'apply)

;; Make html links clickable
(require 'mu4e-contrib)
;; (setq mu4e-html2text-command "textutil -stdin -format html -convert txt -stdout")
(setq mu4e-html2text-command 'mu4e-shr2text)
(add-hook 'mu4e-view-mode-hook
      (lambda()
        ;; try to emulate some of the eww key-bindings, tab/backspace
        (local-set-key (kbd "<tab>") 'shr-next-link)
        (local-set-key (kbd "<backtab>") 'shr-previous-link)))

;; Add option to view message in browser
(add-to-list 'mu4e-view-actions
         '("open in browser" . mu4e-action-view-in-browser) t)

;; Enable inline images and set max width to frame width
(setq
 mu4e-show-images t
 mu4e-image-max-width (frame-text-width)
 mu4e-view-use-gnus t)

;; Use imagemagick, if available
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; Get rid of message buffer after sending an email
(setq message-kill-buffer-on-exit t)

;; Allow paragraphs to reflow
(setq mu4e-compose-format-flowed t)

;; Set Download directory
(setq mu4e-attachment-dir  "~/Downloads")

;; Use mail-add-attachment for adding attachments
;; https://www.reddit.com/r/emacs/comments/ec2oh2/improved_mailaddattachment_to_avoid_exchange_bug/
(defun my-attach-file ()
  (interactive)
  (with-current-buffer (current-buffer)
    (let ((pos (point-marker)))
      (goto-char (point-max))
      (call-interactively 'mail-add-attachment)
      (goto-char pos))))

(define-key mu4e-compose-mode-map (kbd "C-c C-a") 'my-attach-file)

;; Bind e to mu4e-view-save-attachment
(define-key mu4e-view-mode-map (kbd "e") 'mu4e-view-save-attachment)

;; Set gmail style replies
(with-eval-after-load 'message
  (setq
   message-cite-function  'message-cite-original
   message-citation-line-function  'message-insert-formatted-citation-line
   message-cite-reply-position 'above
   message-yank-prefix  "    "
   message-yank-cited-prefix  "    "
   message-yank-empty-prefix  "    "
   message-citation-line-format "\nOn %d %b %Y at %R, %f wrote:\n"))

;; Remap delete to move to trash instead of deleting from server
;; Permanent delete with 'D' move to trash 'd'
;; https://github.com/djcb/mu/issues/1136#issuecomment-486177435
(setf (alist-get 'trash mu4e-marks)
      (list :char '("d" . "▼")
        :prompt "dtrash"
        :dyn-target (lambda (target msg)
              (mu4e-get-trash-folder msg))
        :action (lambda (docid msg target)
              ;; Here's the main difference to the regular trash mark,
              ;; no +T before -N so the message is not marked as
              ;; IMAP-deleted:
              (mu4e~proc-move docid (mu4e~mark-check-target target) "-N"))))

;; Dont reply to self
(setq mu4e-compose-dont-reply-to-self t)

;; Show email address as well as name in message view
(setq mu4e-view-show-addresses 't)

;; Allow fancy icons for mail threads
(setq mu4e-use-fancy-chars t)

;; Icons
(setq mu4e-headers-unread-mark    '("u" . "📩 "))
(setq mu4e-headers-draft-mark     '("D" . "🚧 "))
(setq mu4e-headers-flagged-mark   '("F" . "🚩 "))
(setq mu4e-headers-new-mark       '("N" . "✨ "))
(setq mu4e-headers-passed-mark    '("P" . "↪ "))
(setq mu4e-headers-replied-mark   '("R" . "↩ "))
(setq mu4e-headers-seen-mark      '("S" . " "))
(setq mu4e-headers-trashed-mark   '("T" . "🗑️"))
(setq mu4e-headers-attach-mark    '("a" . "📎 "))
(setq mu4e-headers-encrypted-mark '("x" . "🔑 "))
(setq mu4e-headers-signed-mark    '("s" . "🖊 "))

;; Wrap lines
(add-hook 'mu4e-view-mode-hook #'visual-line-mode)

;; Org Mode for HTML Emails
;; (require 'org-mu4e)
;; convert org mode to HTML automatically
;; (setq org-mu4e-convert-to-html t)

;; Enable spell check for emails
(add-hook 'mu4e-compose-mode-hook
    (defun my-do-compose-stuff ()
       "My settings for message composition."
       (visual-line-mode)
       ;;(org-mu4e-compose-org-mode)
       (use-hard-newlines -1)
       (writegood-mode 1)
       (flyspell-mode)))

;; don't ask when quitting
(setq mu4e-confirm-quit nil)

;; dynamically set column width to size of window
(add-hook 'mu4e-headers-mode-hook
      (defun my/mu4e-change-headers ()
        (interactive)
        (setq mu4e-headers-fields
          `((:human-date . 12)
            (:flags . 4)
            (:from-or-to . 15)
            (:subject . ,(- (window-body-width) 47))
            ;;(:size . 7)
            ))))

;; Use vertical split view for messages
;; (setq mu4e-split-view 'vertical)

;; General emacs mail settings; used when composing e-mail
;; the non-mu4e-* stuff is inherited from emacs/message-mode
(setq mu4e-reply-to-address "amil@mohanan.net"
    user-mail-address "amil@mohanan.net"
    user-full-name  "Amil Mohanan")


;; Message Alerts
;; https://github.com/iqbalansari/mu4e-alert
